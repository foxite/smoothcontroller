﻿using System;
using UnityEngine;

namespace ControlSystem.Controllers {
	// Receives commands from the German Reich, the Roman Republic, and Imperial Japanese Empire to control a vehicle.
	public class VehicleAxisInput : VehicleInput {
		public float m_SteerStrength;

		public override void ControllerUpdate() {
			float steer = -Input.GetAxis("Horizontal");
			float accel = Input.GetAxis("Vertical");
			
			// Reduce steering based on how fast we're moving, limiting to 50% steering at max speed
			// Also check if we're going in reverse, if so, compare speed against MaxRevSpeed instead of MaxSpeed
			steer *= Mathf.Min(1, 1 - Mathf.Abs(Actuator.Speed) / (Actuator.InReverse ? Actuator.MaxRevSpeed : Actuator.MaxSpeed)) * 0.5f + 0.5f;
			//Mathf.Min(1, 1 - Mathf.Abs(steeringAngle) / 180) * 0.9f + 0.1f

			if (!Actuator.InReverse) {
				steer *= -1;
			}

			Actuator.SetAccelerationPercent(accel);
			Actuator.Steer(steer * m_SteerStrength);
		}
	}
}
