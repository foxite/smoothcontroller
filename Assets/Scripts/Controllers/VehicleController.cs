﻿using ControlSystem.Actuators;
using UnityEngine;

namespace ControlSystem.Controllers {
	public abstract class VehicleController : MonoBehaviour {
		protected VehicleActuator Actuator { get; private set; }

		public virtual void ControllerStart(VehicleActuator actuator) {
			Actuator = actuator;
		}

		public virtual void ControllerUpdate() { }
	}
}
