﻿using ControlSystem.Actuators;
using UnityEngine;

namespace ControlSystem.Controllers.AI {
	public class WanderPoint : BehaviourBase {
		public Bounds m_RandomPositionBounds;
		public float m_MaximumDistance;
		public float m_NextTargetDelayMin, m_NextTargetDelayMax;
		
		private float m_NextTargetTimer;
		private Vector3 m_Target;

		public override void ControllerStart(VehicleActuator vehicle) {
			base.ControllerStart(vehicle);
			m_Target = m_RandomPositionBounds.GetRandomPosition();
			m_Target.y = 0.5f;
		}

		protected override bool ShouldMove(Vector3 objectiveDisplacement) {
			bool ret = objectiveDisplacement.magnitude >= m_MaximumDistance;

			if (!ret) {
				m_NextTargetTimer -= Time.deltaTime;
				if (m_NextTargetTimer <= 0) {
					m_Target = m_RandomPositionBounds.GetRandomPosition();
					m_Target.y = 0.5f;
					m_NextTargetTimer = Random.Range(m_NextTargetDelayMin, m_NextTargetDelayMax);
				}
			}
			return ret;
		}

		protected override Vector3 CalculateDisplacement() {
			Vector3 predictedOwnPosition = transform.position + Actuator.Velocity * Actuator.GetBrakeTime();

			return m_Target - predictedOwnPosition;
		}

		private void OnDrawGizmosSelected() {
			Gizmos.color = Color.green;
			Gizmos.DrawWireCube(m_RandomPositionBounds.center, m_RandomPositionBounds.size);

			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(m_Target, 1);
		}
	}
}
