﻿using UnityEngine;

namespace ControlSystem.Controllers.AI {
	public class Seek : BehaviourBase {
		public Transform m_Target;
		public float m_MaximumDistance;

		protected override Vector3 CalculateDisplacement() {
			Vector3 predictedOwnPosition = transform.position + Actuator.Velocity * Actuator.GetBrakeTime();

			return m_Target.position - predictedOwnPosition;
		}

		protected override bool ShouldMove(Vector3 objectiveDisplacement) {
			return objectiveDisplacement.magnitude >= m_MaximumDistance;
		}
	}
}
