﻿using ControlSystem.Actuators;
using UnityEngine;

namespace ControlSystem.Controllers.AI {
	public class FollowPath : BehaviourBase {
		public Transform[] m_Targets;
		public float m_MinimumDistance;
		public bool m_Loop;
		
		private int m_CurrentTargetIndex;
		private bool m_PathFinished;

		public override void ControllerStart(VehicleActuator vehicle) {
			base.ControllerStart(vehicle);

			m_CurrentTargetIndex = 0;
		}

		protected override bool ShouldMove(Vector3 objectiveDisplacement) {
			if (m_PathFinished) {
				return false;
			}

			bool ret = objectiveDisplacement.magnitude >= m_MinimumDistance;

			if (!ret) {
				m_CurrentTargetIndex++;
				if (m_CurrentTargetIndex >= m_Targets.Length) {
					if (m_Loop) {
						m_CurrentTargetIndex = 0;
					} else {
						m_PathFinished = true;
						return false;
					}
				}
			}
			return ret;
		}

		protected override Vector3 CalculateDisplacement() {
			Vector3 predictedOwnPosition = transform.position + Actuator.Velocity * Actuator.GetBrakeTime();

			return m_Targets[m_CurrentTargetIndex].position - predictedOwnPosition;
		}
	}
}
