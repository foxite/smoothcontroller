﻿using ControlSystem.Actuators;
using UnityEngine;

namespace ControlSystem.Controllers.AI {
	public class WanderCourse : BehaviourBase {
		[Min(0)] public float m_OffsetMutationRange;
		
		private Vector3 m_Direction;

		public override void ControllerStart(VehicleActuator vehicle) {
			base.ControllerStart(vehicle);

			m_Direction = transform.forward;
			m_OffsetMutationRange = m_OffsetMutationRange * Mathf.Deg2Rad;
		}

		protected override bool ShouldMove(Vector3 objectiveDisplacement) => true;

		protected override Vector3 CalculateDisplacement() {
			float offset = Random.Range(-m_OffsetMutationRange, m_OffsetMutationRange);
			Vector3 newDirection = m_Direction + new Vector3(Mathf.Cos(offset), 0, Mathf.Sin(offset));
			m_Direction = newDirection.normalized;
			return newDirection;
		}
	}
}
