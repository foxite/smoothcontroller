﻿using System.Collections.Generic;
using System.Linq;
using ControlSystem.Actuators;
using UnityEngine;

namespace ControlSystem.Controllers.AI {
	public class Hide : Seek {
		public float m_TimeBetweenChecks;
		public VehicleActuator m_HideFrom;

		private Transform[] m_Obstacles;
		private float m_TimeToNextCheck;
		private Transform m_HideLocation;

		public override void ControllerStart(VehicleActuator actuator) {
			base.ControllerStart(actuator);

			m_Obstacles = (from go in FindObjectsOfType<GameObject>()
						   where go.isStatic && go.layer == LayerMask.NameToLayer("Obstacle")
						   select go.transform).ToArray();

			m_HideLocation = new GameObject(name + " Hiding location").transform;

			m_Target = m_HideLocation;
		}

		public override void ControllerUpdate() {
			m_TimeToNextCheck -= Time.deltaTime;
			if (m_TimeToNextCheck <= 0) {
				m_TimeToNextCheck = m_TimeBetweenChecks;

				if (Physics.Raycast(m_HideFrom.transform.position, transform.position - m_HideFrom.transform.position, out RaycastHit hitInfo) && hitInfo.transform == transform) {
					// Select new hiding spot
					Vector3[] hidingSpots = m_Obstacles.Select(obstacle => obstacle.position + (obstacle.position - m_HideFrom.transform.position).normalized * 3).ToArray();

					int closestSpotIndex = 0;
					float closestSpotDistance = Vector3.Distance(hidingSpots[0], transform.position);
					for (int i = 1; i < hidingSpots.Length; i++) {
						float nextDistance = Vector3.Distance(hidingSpots[i], transform.position);
						if (nextDistance < closestSpotDistance) {
							closestSpotIndex = i;
							closestSpotDistance = nextDistance;
						}
					}

					m_HideLocation.transform.position = hidingSpots[closestSpotIndex];
				}
			}

			base.ControllerUpdate();
		}
	}
}
