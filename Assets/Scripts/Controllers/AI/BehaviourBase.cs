﻿using UnityEngine;

namespace ControlSystem.Controllers.AI {
	public abstract class BehaviourBase : VehicleController {
		/// <summary>
		/// Causes the controller to slow down then trying to steer hard. When this is turned off, steering will be limited at high velocities.
		/// </summary>
		public bool m_SteeringBrake = true;

		public override void ControllerUpdate() {
			Vector3 displacement = CalculateDisplacement();

			if (ShouldMove(displacement)) {
				float steeringAngle = CalculateSteeringAngle(displacement);
				float desiredSpeed = CalculateDesiredSpeed(displacement);

				if (m_SteeringBrake) {
					// Slow down based on how hard we're trying to steer, limiting speed to 10% when we're headed the opposite way
					desiredSpeed *= Mathf.Min(1, 1 - Mathf.Abs(steeringAngle) / 180) * 0.9f + 0.1f;
				} else {
					// Reduce steering based on how fast we're moving, limiting to 10% at maximum speed
					float maxSpeed = Mathf.Max(Actuator.MaxSpeed, Actuator.MaxRevSpeed);
					float factor = Mathf.Abs(Actuator.Speed) / maxSpeed * 0.9f + 0.1f;
					steeringAngle *= factor;
				}
				float accel = desiredSpeed - Actuator.Speed;

				Actuator.SetAcceleration(accel);
				Actuator.Steer(steeringAngle);
			} else if (Actuator.Velocity.sqrMagnitude > 0) {
				Actuator.SetAcceleration(-Actuator.MaxBrake);
				Actuator.Steer(0);
			} else {
				Actuator.SetAcceleration(0);
				Actuator.Steer(0);
			}
		}

		protected virtual float CalculateDesiredSpeed(Vector3 displacement) {
			return displacement.magnitude;
		}

		protected virtual float CalculateSteeringAngle(Vector3 displacement) {
			return Vector3.SignedAngle(Actuator.Velocity.normalized, displacement, Vector3.up);
		}

		protected abstract Vector3 CalculateDisplacement();
		protected abstract bool ShouldMove(Vector3 displacement);
	}
}
