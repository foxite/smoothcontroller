﻿using UnityEngine;

namespace ControlSystem.Controllers.AI {
	public class Flee : BehaviourBase {
		public Transform m_Avoid;
		public float m_MinimumDistance;

		protected override Vector3 CalculateDisplacement() {
			Vector3 predictedOwnPosition = transform.position + Actuator.Velocity * Actuator.GetBrakeTime();

			return predictedOwnPosition - m_Avoid.position;
		}

		protected override bool ShouldMove(Vector3 objectiveDisplacement) {
			return objectiveDisplacement.magnitude <= m_MinimumDistance;
		}
	}
}
