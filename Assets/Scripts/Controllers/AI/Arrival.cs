﻿using UnityEngine;

namespace ControlSystem.Controllers.AI {
	// Not exactly sure what the point of this is. This will reduce speed as it approaches the target.
	// The effect is that it will get very close to the target and then pole-dance around it.
	public class Arrival : BehaviourBase {
		public Transform m_Target;
		public float m_DecelTime;

		protected override Vector3 CalculateDisplacement() {
			return m_Target.position - transform.position;
		}

		protected override bool ShouldMove(Vector3 displacement) {
			return true;
		}

		protected override float CalculateDesiredSpeed(Vector3 displacement) {
			return Mathf.Min(Actuator.MaxSpeed, displacement.magnitude / m_DecelTime);
		}
	}
}
