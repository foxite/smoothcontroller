﻿using ControlSystem.Actuators;
using UnityEngine;

namespace ControlSystem.Controllers.AI {
	public class Pursuit : BehaviourBase {
		public VehicleActuator m_Target;
		public float m_LookaheadTime = 1.0f;
		public float m_MaximumDistance;

		protected override Vector3 CalculateDisplacement() {
			Vector3 predictedTargetPosition = m_Target.Velocity * m_LookaheadTime + m_Target.transform.position;
			Vector3 predictedOwnPosition = transform.position + Actuator.Velocity * m_LookaheadTime;

			return predictedTargetPosition - predictedOwnPosition;
		}

		protected override float CalculateDesiredSpeed(Vector3 displacement) {
			return Actuator.MaxSpeed;
		}

		protected override bool ShouldMove(Vector3 objectiveDisplacement) {
			return objectiveDisplacement.magnitude >= m_MaximumDistance;
		}
	}
}
