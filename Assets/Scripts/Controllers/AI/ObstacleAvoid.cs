﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ControlSystem.Controllers.AI {
	public class ObstacleAvoid : VehicleController {
		public float LookDistance;

		private const float Factor = 2f;

		private float m_SteeringOffset;
		private Transform m_Avoid;
		private int m_ObstacleLayer;
		private float turn;

		private void Awake() {
			m_ObstacleLayer = 1 << LayerMask.NameToLayer("Obstacle");
		}

		public override void ControllerUpdate() {
			if (Actuator.Velocity.sqrMagnitude > 0 && Physics.BoxCast(transform.position, Vector3.one, Actuator.Velocity, out RaycastHit hit, transform.rotation, LookDistance, m_ObstacleLayer)) {
				float angle = Vector3.SignedAngle(Actuator.Velocity.normalized, hit.transform.position - transform.position, Vector3.up);

				turn = angle - Mathf.Sign(angle) * 90;
				Actuator.Steer(turn * Factor);
			}
		}
	}
}
