﻿using ControlSystem.Controllers;

namespace ControlSystem.Arenas {
	public class SingleControllerBrain : Brain {
		protected override void Awake() {
			base.Awake();
			CurrentController = GetComponent<VehicleController>();
		}
	}
}
