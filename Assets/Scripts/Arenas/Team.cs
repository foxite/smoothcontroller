﻿using UnityEngine;

namespace ControlSystem.Arenas {
	[CreateAssetMenu]
	public class Team : ScriptableObject {
		public string m_TeamName;
		public Color m_Color;
		public Material m_PaintMaterial;
	}
}
