﻿using System;
using ControlSystem.Actuators;
using ControlSystem.Controllers;
using UnityEngine;

namespace ControlSystem.Arenas {
	public abstract class Brain : MonoBehaviour {
		[SerializeField] private float m_Health;
		[SerializeField] protected Team m_Team;

		private VehicleController m_CurrentController;

		protected VehicleController CurrentController {
			get => m_CurrentController;
			set {
				m_CurrentController = value;
				CurrentController.ControllerStart(Actuator);
			}
		}

		public VehicleActuator Actuator { get; private set; }
		public Team PartOfTeam => m_Team;

		public float Health {
			get => m_Health;
			set {
				m_Health = value;
				if (m_Health <= 0) {
					Die();
				}
			}
		}

		protected virtual void Die() {
			Destroy(this);
		}

		protected virtual void Awake() {
			Actuator = GetComponent<VehicleActuator>();
			TeamManager.Instance.AddToTeam(this);
		}

		protected virtual void Start() { }

		protected virtual void LateUpdate() {
			CurrentController.ControllerUpdate();
		}
	}
}
