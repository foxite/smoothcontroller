﻿using System.Collections.Generic;
using UnityEngine;

namespace ControlSystem.Arenas.Defense {
	public abstract class ThiefBrain : Brain {
		protected IEnumerable<Brain> m_AllEnemies;

		protected override void Awake() {
			base.Awake();
		}

		protected override void Start() {
			base.Start();
			
			m_AllEnemies = TeamManager.Instance.GetAllEnemies(m_Team);
		}
	}
}
