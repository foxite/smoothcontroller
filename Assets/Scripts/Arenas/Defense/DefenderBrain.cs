﻿using ControlSystem.Actuators;
using ControlSystem.Controllers.AI;
using UnityEngine;

namespace ControlSystem.Arenas.Defense {
	/// <summary>
	/// The DefenderBrain will stay close to a certain object and will attack enemies that get too close.
	/// </summary>
	public class DefenderBrain : GuardBrain {
		[SerializeField] private float m_MaximumDistanceToDefendPoint;
		[SerializeField] private float m_MaximumDistanceToPursuedEnemy;

		private Seek m_Seek;
		private Pursuit m_Pursuit;

		protected override void Awake() {
			base.Awake();

			m_Seek = GetComponent<Seek>();
			m_Pursuit = GetComponent<Pursuit>();

			m_Seek.m_Target = m_DefendPoint;
			m_Seek.m_MaximumDistance = m_MaximumDistanceToDefendPoint;

			m_Pursuit.m_MaximumDistance = m_MaximumDistanceToPursuedEnemy;

			CurrentController = m_Seek;

			m_SentryTower.EnemyLeftRange += OnEnemyLeftRange;
			m_SentryTower.EnemyEnteredRange += OnEnemyEnteredRange;
		}

		private void OnEnemyEnteredRange(Brain enemy) {
			if (CurrentController == m_Seek) {
				m_Pursuit.m_Target = enemy.Actuator;
				CurrentController = m_Pursuit;
			}
		}

		private void OnEnemyLeftRange(Brain enemy) {
			if (CurrentController == m_Pursuit && m_Pursuit.m_Target == enemy.Actuator) {
				CurrentController = m_Seek;
			}
		}

		private void Update() {
			if (CurrentController == m_Pursuit) {
				m_Pursuit.m_MaximumDistance = m_MaximumDistanceToDefendPoint - Vector3.Distance(m_DefendPoint.position, m_Pursuit.m_Target.transform.position) + 1;
			}
		}
	}
}
