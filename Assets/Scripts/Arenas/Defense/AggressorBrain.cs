﻿using ControlSystem.Controllers.AI;
using UnityEngine;

namespace ControlSystem.Arenas.Defense {
	/// <summary>
	/// The AggressorBrain will guard a certain object and attack enemies that gets too close. It will pursue enemies that try to escape.
	/// </summary>
	public class AggressorBrain : GuardBrain {
		[SerializeField, Tooltip("Distance from the point at which this Brain will return to the point it should defend.")] private float m_PersistenceRange;
		[SerializeField, Tooltip("Brain will return to the point when the pursued enemy gets out of this range from this brain.")] private float m_GiveUpRange;
		[SerializeField] private float m_MaximumDistanceToDefendPoint;
		[SerializeField] private float m_MaximumDistanceToPursuedEnemy;

		private Seek m_Seek;
		private Pursuit m_Pursuit;

		protected override void Awake() {
			base.Awake();

			m_Seek = GetComponent<Seek>();
			m_Pursuit = GetComponent<Pursuit>();

			m_Seek.m_Target = m_DefendPoint;
			m_Seek.m_MaximumDistance = m_MaximumDistanceToDefendPoint;

			m_Pursuit.m_MaximumDistance = m_MaximumDistanceToPursuedEnemy;

			CurrentController = m_Seek;

			m_SentryTower.EnemyEnteredRange += OnEnemyEnteredRange;

			// Square the range values. Doing this allows us to skip the square root calculation that Vector3.Distance() does.
			// We simply calculate the square magnitude of the difference between two points, and compare that against our squared range values.
			m_PersistenceRange *= m_PersistenceRange;
			m_GiveUpRange *= m_GiveUpRange;
		}

		private void OnEnemyEnteredRange(Brain enemy) {
			if (CurrentController == m_Seek) {
				m_Pursuit.m_Target = enemy.Actuator;
				CurrentController = m_Pursuit;
			}
		}

		private void Update() {
			if (CurrentController == m_Pursuit) {
				if ((transform.position - m_Pursuit.m_Target.transform.position).sqrMagnitude > m_GiveUpRange &&
					(transform.position - m_DefendPoint.position).sqrMagnitude > m_PersistenceRange) {
					CurrentController = m_Seek;
				} else {
					m_Pursuit.m_MaximumDistance = m_MaximumDistanceToDefendPoint - Vector3.Distance(m_DefendPoint.position, m_Pursuit.m_Target.transform.position) + 1;
				}
			}
		}
	}
}
