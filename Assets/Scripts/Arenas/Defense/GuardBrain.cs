﻿using System.Collections.Generic;
using UnityEngine;

namespace ControlSystem.Arenas.Defense {
	public abstract class GuardBrain : Brain {
		[SerializeField] protected Transform m_DefendPoint;
		[SerializeField] protected SentryTower m_SentryTower;

		protected IEnumerable<Brain> m_AllEnemies;

		protected override void Start() {
			base.Start();

			m_AllEnemies = TeamManager.Instance.GetAllEnemies(m_Team);
		}
	}
}
