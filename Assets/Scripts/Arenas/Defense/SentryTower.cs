﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ControlSystem.Arenas.Defense {
	public class SentryTower : MonoBehaviour {
		[SerializeField] private Team m_Team;
		[SerializeField] private Transform m_DefendPoint;
		[SerializeField] private float m_EnterRange;
		[SerializeField] private float m_ExitRange;
		[SerializeField] private int m_EnemiesCheckedPerFrame;

		private IEnumerable<Brain> m_Enemies;
		private IEnumerable<Brain> m_Allies;
		private HashSet<Brain> m_EnemiesInRange; // According to this, https://stackoverflow.com/a/823876 HashSet<T> is the best choice if we need a fast Contains() check.

		public event Action<Brain> EnemyEnteredRange;
		public event Action<Brain> EnemyLeftRange;

		private void Start() {
			m_Enemies = TeamManager.Instance.GetAllEnemies(m_Team);
			m_Allies = TeamManager.Instance.GetTeamMembers(m_Team);

			m_EnemiesInRange = new HashSet<Brain>();

			// Square the range values. Doing this allows us to skip the square root calculation that Vector3.Distance() does.
			// We simply calculate the square magnitude of the difference between two points, and compare that against our squared range values.
			m_EnterRange *= m_EnterRange;
			m_ExitRange *= m_ExitRange;

			StartCoroutine(ScanEnemies());
		}

		private IEnumerator ScanEnemies() {
			int scannedEnemies = 0;
			while (true) {
				/* Scan for enemies within range of the point
				 * This can take quite a while if there's many enemies involved, so we spread it out over multiple frames using a coroutine.
				 * 
				 * Another way you could reduce the load generated by this loop is by running it once every 0.2 seconds, for example.
				 * However, that could cause stuttering as some frames will take significantly longer to process than others.
				 * By spreading it out like this, we evenly distribute the load over each frame, preventing stutters.
				 * 
				 * Another way it might be possible is to run this loop on a seperate thread, but I'm not sure if you can read Transform.position from other threads.
				 * I know you definitely can't *write* anything that has to do with Unity, so to avoid Brains from having to deal with this problem,
				 *  you could keep a list of all events you need to fire, which is updated by the scanning thread, and processed by the Unity thread.
				 * If you would do this, you shouldn't use a List<T>, but a ConcurrentBag<T> or some other ConcurrentCollection, as these will prevent things like
				 *  race conditions (where one thread updates the list while another is reading from it, causing inconsistent results and possibly data corruption)
				 *  and other threading-related problems that I don't know much about.
				 * 
				 * All things considered I decided that sticking to coroutines (which is a bit like threading except not really) is the easiest and probably the best solution.
				 */
				foreach (Brain item in m_Enemies) {
					if (scannedEnemies >= m_EnemiesCheckedPerFrame) {
						// This block used to be at the end of the for loop, but I added another yield after the for loop.
						// I moved this up here so that it wouln't yield twice in a row if this happened at the last iteration of the loop.
						scannedEnemies = 0;
						yield return null; // Continue execution in the next frame.
					}

					if (m_EnemiesInRange.Contains(item)) {
						if ((item.transform.position - m_DefendPoint.position).sqrMagnitude > m_ExitRange) {
							m_EnemiesInRange.Remove(item);
							EnemyLeftRange?.Invoke(item);
						}
					} else {
						if ((item.transform.position - m_DefendPoint.position).sqrMagnitude <= m_EnterRange) {
							m_EnemiesInRange.Add(item);
							EnemyEnteredRange?.Invoke(item);
						}
					}

					scannedEnemies++;
				}
				yield return null;
			}
		}
	}
}
