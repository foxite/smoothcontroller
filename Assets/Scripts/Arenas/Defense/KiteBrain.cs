﻿using System.Linq;
using ControlSystem.Controllers.AI;
using UnityEngine;

namespace ControlSystem.Arenas.Defense {
	public class KiteBrain : ThiefBrain {
		[SerializeField] private Transform m_Point;
		[SerializeField] private float m_MinimumPointDistanceDefender;
		[SerializeField] private float m_MinimumPointDistanceAggressor;
		[SerializeField] private float m_MinimumEnemyDistance;
		[SerializeField] private float m_MaximumKiteDistance;

		private Pursuit m_Pursuit;
		private Evade m_Evade;
		private float m_CurrentMinimumPointDistance;

		protected override void Awake() {
			base.Awake();

			m_Pursuit = GetComponent<Pursuit>();
			m_Evade = GetComponent<Evade>();

			m_Pursuit.m_MaximumDistance =  m_MaximumKiteDistance * 0.9f;
			m_Evade.m_MinimumDistance = m_MinimumPointDistanceDefender * 1.1f;

			void square(ref float value) {
				value *= value;
			}

			square(ref m_MinimumPointDistanceDefender);
			square(ref m_MinimumPointDistanceAggressor);
			square(ref m_MinimumEnemyDistance);
			square(ref m_MaximumKiteDistance);

			m_Evade.m_SteeringBrake = false;
		}

		protected override void Start() {
			base.Start();

			KiteNewTarget();
		}

		private void Update() {
			if (CurrentController == m_Evade) {
				if ((transform.position - m_Evade.m_Target.transform.position).sqrMagnitude > m_MinimumEnemyDistance &&
					(transform.position - m_Point.position).sqrMagnitude > m_CurrentMinimumPointDistance) {
					KiteNewTarget();
				}
			} else if (CurrentController == m_Pursuit) {
				if ((transform.position - m_Pursuit.m_Target.transform.position).sqrMagnitude <= m_MaximumKiteDistance) {
					m_Evade.m_Target = m_Pursuit.m_Target;
					CurrentController = m_Evade;
				}
			}
		}

		private void KiteNewTarget() {
			Brain selectedEnemy = m_AllEnemies.ElementAt(Random.Range(0, m_AllEnemies.Count()));
			m_Pursuit.m_Target = selectedEnemy.Actuator;
			CurrentController = m_Pursuit;

			if (selectedEnemy is AggressorBrain) {
				m_CurrentMinimumPointDistance = m_MinimumPointDistanceAggressor;
			} else if (selectedEnemy is DefenderBrain) {
				m_CurrentMinimumPointDistance = m_MinimumPointDistanceDefender;
			} else {
				Debug.LogError("Selected enemy is of type " + selectedEnemy.GetType().Name + " which is not recognized", this);
			}
		}
	}
}
