﻿using System.Collections.Generic;
using System.Linq;

namespace ControlSystem.Arenas {
	public class TeamManager : Singleton<TeamManager> {
		private Dictionary<Team, List<Brain>> m_BrainsByTeam = new Dictionary<Team, List<Brain>>();

		public void AddToTeam(Brain brain) {
			if (m_BrainsByTeam.TryGetValue(brain.PartOfTeam, out List<Brain> value)) {
				value.Add(brain);
			} else {
				m_BrainsByTeam[brain.PartOfTeam] = new List<Brain>() {
					brain
				};
			}
		}
		
		public IEnumerable<Brain> GetTeamMembers(Team team) {
			//m_BrainsByTeam.TryGetValue(team, out List<BrainBase> ret);
			//return ret;
			return m_BrainsByTeam.Where(kvp => kvp.Key == team).Single().Value;
		}

		public IEnumerable<Brain> GetAllEnemies(Team ofTeam) {
			return m_BrainsByTeam.Where(kvp => kvp.Key != ofTeam).SelectMany(kvp => kvp.Value);
		}
	}
}
