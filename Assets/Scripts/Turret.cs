﻿using System.Collections.Generic;
using System.Linq;
using ControlSystem.Actuators;
using ControlSystem.Arenas;
using UnityEngine;

namespace ControlSystem {
	public class Turret : MonoBehaviour {
		[SerializeField] private ObjectPool m_PaintballPool;
		[SerializeField] private float m_TimeBetweenShots = 0.5f;
		[SerializeField, Tooltip("Time between target scans while already fighting.")] private float m_TimeBetweenTargetScans = 5f;
		[SerializeField, Tooltip("Time between target scans while not firing at a target.")] private float m_TimeBetweenActiveTargetScans = 1f;
		[SerializeField] private float m_Range = 10;
		[SerializeField] private Transform m_PaintballOrigin;
		[SerializeField] private float m_TurretRotationSpeed;
		
		private Brain m_Brain;
		private VehicleActuator m_Target;
		private IEnumerable<Brain> m_AllEnemies;
		private float m_Timer;

		private void Awake() {
			m_Brain = transform.parent.GetComponent<Brain>();

			m_TurretRotationSpeed *= Mathf.Deg2Rad;
		}

		private void Start() {
			m_AllEnemies = TeamManager.Instance.GetAllEnemies(m_Brain.PartOfTeam);
		}

		private void Update() {
			m_Timer -= Time.deltaTime;
			if (m_Timer <= 0) {
				if (m_Target == null) {
					m_Timer = m_TimeBetweenActiveTargetScans;
					// Scan
					// TODO
					Brain closestEnemy = m_AllEnemies.First();
					float closestDistance = Vector3.Distance(closestEnemy.transform.position, transform.position);
					foreach (Brain enemy in m_AllEnemies) {
						float dist = Vector3.Distance(enemy.transform.position, transform.position);
						if (dist > closestDistance) {
							closestDistance = dist;
							closestEnemy = enemy;
						}
					}
					m_Target = closestEnemy.Actuator;
				} else {
					// Fire
					m_Timer = m_TimeBetweenShots;

					Vector3 oldForward = transform.forward;
					transform.forward = Vector3.RotateTowards(transform.forward, transform.position - m_Target.transform.position, m_TurretRotationSpeed, 1);

					if (oldForward == transform.forward) {
						m_PaintballPool.TakeFromPool(m_PaintballOrigin.position, m_PaintballOrigin.forward, m_Brain.PartOfTeam);
					}
				}
			}
		}

		// Slow
		private VehicleActuator GetNewTarget() {
			float sqrDistance(Brain brain) {
				return (transform.position - brain.transform.position).sqrMagnitude;
			}

			return m_AllEnemies.Aggregate((curMin, x) => curMin == null || sqrDistance(x) < sqrDistance(curMin) ? x : curMin).Actuator;
		}
	}
}
