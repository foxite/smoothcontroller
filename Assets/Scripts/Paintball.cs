﻿using ControlSystem.Arenas;
using UnityEngine;

namespace ControlSystem {
	public class Paintball : PooledObject {
		[SerializeField] private float m_Speed;

		private MeshRenderer m_MeshRenderer;
		private Vector3 m_Velocity;
		private Team m_ShotByTeam;

		private void OnEnable() {
			m_MeshRenderer = GetComponent<MeshRenderer>();
			m_MeshRenderer.material = m_ShotByTeam.m_PaintMaterial;
		}

		private void FixedUpdate() {
			transform.position += m_Velocity * Time.fixedDeltaTime;
		}

		private void OnCollisionEnter(Collision collision) {
			if (collision.gameObject.HasComponent<Brain>(out Brain brain)) {
				Destroy(this);
				brain.Health -= 5;
			}
		}

		public override void OnReturnToPool(ObjectPool pool) { }

		/// <param name="params_">Expected parameters: Vector3 direction, Team shotByTeam</param>
		public override void OnTakeFromPool(ObjectPool pool, Vector3 position, params object[] params_) {
			transform.position = position;
			m_Velocity = ((Vector3) params_[0]) * m_Speed;
			m_ShotByTeam = (Team) params_[1];
		}
	}
}
