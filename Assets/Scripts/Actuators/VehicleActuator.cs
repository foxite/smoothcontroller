﻿using ControlSystem.Controllers;
using UnityEngine;

namespace ControlSystem.Actuators {
	[DisallowMultipleComponent]
	public class VehicleActuator : MonoBehaviour {
		[SerializeField] private float m_MaxTurn;
		[SerializeField] private float m_MaxSpeed;
		[SerializeField] private float m_MaxRevSpeed;
		[SerializeField] private float m_MaxAcceleration;
		[SerializeField] private float m_MaxBrake;
		[SerializeField] private float m_TimeToReverse;

		private float m_Steer = 0;
		private float m_Acceleration = 0;
		private float m_ReversingTime = 0;
		private bool m_Reversing = false;
		
		public Vector3 Velocity => transform.forward * Speed;
		public float Speed { get; private set; } = 0;

		public float MaxTurn => m_MaxTurn;
		public float MaxSpeed => m_MaxSpeed;
		public float MaxRevSpeed => m_MaxRevSpeed;
		public float MaxAcceleration => m_MaxAcceleration;
		public float MaxBrake => m_MaxBrake;
		public bool InReverse => Speed < 0;

		public virtual void SetAccelerationPercent(float percent) {
			if (Mathf.Sign(Speed) != Mathf.Sign(percent)) {
				SetAcceleration(MaxBrake * percent);
			} else {
				SetAcceleration(MaxAcceleration * percent);
			}
		}

		public virtual void SetAcceleration(float accel) {
			if (Speed < 0) {
				if (accel > 0) {
					// Increase m_Speed using MaxBrake
					m_Acceleration = Mathf.Min(m_MaxBrake, Mathf.Abs(accel));
				} else {
					// Decrease m_Speed using MaxAcceleration
					m_Acceleration = -Mathf.Min(m_MaxAcceleration, Mathf.Abs(accel));
				}
			} else {
				if (accel > 0) {
					// Increase m_Speed using MaxAcceleration
					m_Acceleration = Mathf.Min(m_MaxAcceleration, Mathf.Abs(accel));
				} else {
					// Decrease m_Speed using MaxBrake
					m_Acceleration = -Mathf.Min(m_MaxBrake, Mathf.Abs(accel));
				}
			}
		}

		public virtual void Steer(float turn) {
			m_Steer = Mathf.Min(MaxTurn, Mathf.Abs(turn)) * Mathf.Sign(turn); // * Mathf.Abs(m_Speed / m_MaxSpeed);
		}

		protected virtual void FixedUpdate() {
			if (m_Reversing) {
				m_ReversingTime -= Time.fixedDeltaTime;
				if (m_ReversingTime < 0) {
					m_Reversing = false;
				}
			} else {
				float prevSign = Mathf.Sign(Speed);

				Speed += m_Acceleration * Time.fixedDeltaTime;
				Speed = Mathf.Clamp(Speed, -MaxRevSpeed, MaxSpeed);

				if (Mathf.Sign(Speed) != prevSign && prevSign != 0) {
					m_ReversingTime = m_TimeToReverse;
					m_Reversing = true;
					Speed = 0;
				}

				/*float angle = Mathf.Atan2(transform.forward.z, transform.forward.x);
				angle += m_Steer * Time.fixedDeltaTime;
				transform.forward = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle));*/
				transform.Rotate(0, m_Steer * Time.fixedDeltaTime, 0);

				transform.Translate(0, 0, Speed * Time.fixedDeltaTime, Space.Self);
			}
		}

		public float GetBrakeTime() {
			return Mathf.Abs(Speed / m_MaxBrake);
		}
	}
}
