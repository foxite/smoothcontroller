﻿using System.Collections.Generic;
using System.Linq;
using ControlSystem.Actuators;
using ControlSystem.Controllers;
using UnityEngine;

namespace Assets.Scripts.Actuators {
	public class MultiControllerActuator : VehicleActuator {
		[SerializeField] private VehicleController[] m_AlwaysActive;

		private List<float> m_AccelerationCalls;
		private List<float> m_SteeringCalls;

		private void Awake() {
			m_AccelerationCalls = new List<float>();
			m_SteeringCalls = new List<float>();
		}

		private void Start() {
			for (int i = 0; i < m_AlwaysActive.Length; i++) {
				m_AlwaysActive[i].ControllerStart(this);
			}
		}

		public override void SetAcceleration(float accel) {
			m_AccelerationCalls.Add(accel);
		}

		public override void Steer(float turn) {
			m_SteeringCalls.Add(turn);
		}

		private void Update() {
			for (int i = 0; i < m_AlwaysActive.Length; i++) {
				m_AlwaysActive[i].ControllerUpdate();
			}
		}

		private void LateUpdate() {
			if (m_AccelerationCalls.Count != 0) {
				base.SetAcceleration(m_AccelerationCalls.Average());
				m_AccelerationCalls.Clear();
			}
			if (m_SteeringCalls.Count != 0) {
				base.Steer(m_SteeringCalls.Average());
				m_SteeringCalls.Clear();
			}

		}
	}
}
